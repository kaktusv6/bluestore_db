-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 15 2017 г., 01:26
-- Версия сервера: 10.1.28-MariaDB
-- Версия PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bluestore`
--
CREATE DATABASE IF NOT EXISTS `bluestore` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bluestore`;

DELIMITER $$
--
-- Процедуры
--
DROP PROCEDURE IF EXISTS `addUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `addUser` (IN `login` VARCHAR(255), IN `name` VARCHAR(255), IN `second_name` VARCHAR(255), IN `tel` VARCHAR(255), IN `country` VARCHAR(255), IN `city` VARCHAR(255), IN `address` TEXT, IN `password` TEXT, IN `status` BINARY(1))  NO SQL
INSERT INTO users 
(
  login,
  name,
  second_name,
  tel,
  country,
  city,
  address,
  password,
  status)
VALUES (login,
  name,
  second_name,
  tel,
  country,
  city,
  address,
  password,
  status)$$

DROP PROCEDURE IF EXISTS `getAllUsers`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllUsers` ()  NO SQL
    COMMENT 'Получение всех пользователей'
select * from users$$

DROP PROCEDURE IF EXISTS `getCategories`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCategories` ()  NO SQL
    COMMENT 'Получение всех категорий'
select * from categories$$

DROP PROCEDURE IF EXISTS `getCategory`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCategory` (IN `id` INT(11))  NO SQL
    COMMENT 'Получение конкретного раздела по id'
select * from categories where categories.id = id$$

DROP PROCEDURE IF EXISTS `getItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getItem` (IN `id` INT(11))  NO SQL
    COMMENT 'Получение товара по id'
select * from items where items.id = id$$

DROP PROCEDURE IF EXISTS `getItemsCategory`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getItemsCategory` (IN `id_category` INT(11))  NO SQL
    COMMENT 'Получение товаров по id категории'
select * from items where items.id_category = id_category$$

DROP PROCEDURE IF EXISTS `getUserById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserById` (IN `id` INT(11))  NO SQL
    COMMENT 'Получение данных пользователя по id'
SELECT * from users where users.id = id$$

DROP PROCEDURE IF EXISTS `getUserByLogin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByLogin` (IN `login` VARCHAR(255))  NO SQL
    COMMENT 'Получение данных пользователя по login'
select * from users where users.login = login$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `baskets`
--

DROP TABLE IF EXISTS `baskets`;
CREATE TABLE IF NOT EXISTS `baskets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Индекс раздела',
  `name` varchar(255) NOT NULL COMMENT 'Наименование раздела',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Section 1'),
(2, 'Section 2'),
(3, 'Section 3');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `messege` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Индекс товара',
  `id_category` int(11) NOT NULL COMMENT 'Индекс раздела',
  `name` varchar(255) NOT NULL COMMENT 'Имя товара',
  `description` text NOT NULL COMMENT 'Описание товара',
  `path_image` text COMMENT 'Путь до изображения товара',
  `price` int(11) NOT NULL COMMENT 'Цена товара',
  `count` int(11) NOT NULL COMMENT 'Кол-во товара',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `id_category`, `name`, `description`, `path_image`, `price`, `count`) VALUES
(1, 1, 'item 1', 'description description description description description description description description description ', NULL, 1200, 20),
(2, 1, 'item 2', 'desc desc desc desc desc desc desc desc desc ', NULL, 200, 100),
(3, 2, 'item 2/2', 'descdesc descdesc descdesc descdesc descdesc descdesc descdesc descdesc descdesc descdesc descdesc nastya lisica(fox)', NULL, 1500, 200);

-- --------------------------------------------------------

--
-- Структура таблицы `link_basket_iems`
--

DROP TABLE IF EXISTS `link_basket_iems`;
CREATE TABLE IF NOT EXISTS `link_basket_iems` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Индекс связи',
  `id_basket` int(11) NOT NULL COMMENT 'Индекс корзины',
  `id_item` int(11) NOT NULL COMMENT 'Индекс товара',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Индекс заказа',
  `id_user` int(11) NOT NULL COMMENT 'Индекс пользователя',
  `id_basket` int(11) NOT NULL COMMENT 'Индекс корзины',
  `date` date NOT NULL COMMENT 'Дата заказа',
  `status` varchar(255) NOT NULL COMMENT 'Статус заказа',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Индекс пользователя',
  `login` varchar(255) NOT NULL COMMENT 'Логин пользователя',
  `name` varchar(255) NOT NULL COMMENT 'Имя пользователя',
  `second_name` varchar(255) NOT NULL COMMENT 'Фамилия пользователя',
  `tel` varchar(255) NOT NULL COMMENT 'Телефон пользователя',
  `country` varchar(255) NOT NULL COMMENT 'Страна пользователя',
  `city` varchar(255) NOT NULL COMMENT 'Город пользователя',
  `address` text NOT NULL COMMENT 'Адрес пользователя',
  `password` text NOT NULL COMMENT 'Пароль пользователя',
  `status` binary(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `name`, `second_name`, `tel`, `country`, `city`, `address`, `password`, `status`) VALUES
(2, 'test@test.test', 'test', '', '', '', '', '', '098f6bcd4621d373cade4e832627b4f6', 0x30),
(3, 'tokhtina.as@yandex.ru', 'ÐÐ½Ð°ÑÑ‚Ð°ÑÐ¸Ñ', '', '', '', '', '', 'f970e2767d0cfe75876ea857f92e319b', 0x31),
(4, 'nokvel96@gmail.com', 'Ð”Ð°Ð½Ñ', '', '', '', '', '', '81dc9bdb52d04dc20036dbd8313ed055', 0x31),
(6, 'kaktusv6@gmail.com', 'vasya', '', '', '', '', '', '9cdfb439c7876e703e307864c9167a15', 0x31);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
